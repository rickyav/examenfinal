#pragma once
class Fecha
{
private:
	int mes;
	int dia;
public:
	Fecha();
	~Fecha();
	int getDia();
	int getMes();
	void setDia(int);
	void setMes(int);
};

