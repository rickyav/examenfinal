#include <iostream>
#include "Fecha.h"
#include "Empleado.h"
using namespace std;
int main()
{
	Fecha fecha1;
	Fecha fecha2;

	Empleado emp1;
	Empleado emp2;
	
	fecha1.setDia(19);
	fecha1.setMes(5);
	
	emp1.setNombreEmp("Pepe");
	emp1.setSueldo(10000);
	emp1.setCategoriaEmp(1);
	emp1.setFecha(fecha1);

	emp2.setNombreEmp("Rocio");
	emp2.setSueldo(12000);
	emp2.setCategoriaEmp(2);
	emp2.setFecha(fecha2);

	cout << "El empleado " << emp1.getNombreEmpleado() << " tiene un sueldo de " << emp1.getSueldoEmp() << " con categoria " << emp1.getCategoriaEmp() << " y su cumpleanios es el " << emp1.getFecha().getDia() << " del mes " << emp1.getFecha().getMes() << "\n";
	cout << "El empleado " << emp2.getNombreEmpleado() << " tiene un sueldo de " << emp2.getSueldoEmp() << " con categoria " << emp2.getCategoriaEmp() << " y su cumpleanios es el " << emp2.getFecha().getDia() << " del mes " << emp2.getFecha().getMes() << "\n";
	
	emp1.promoverEmpleado(4);
	cout << "\nSe promovio " << emp1.getNombreEmpleado() << "\n\n";
	
	cout << "El empleado " << emp1.getNombreEmpleado() << " tiene un sueldo de " << emp1.getSueldoEmp() << " con categoria " << emp1.getCategoriaEmp() << "\n\n";
	cout << "El empleado " << emp2.getNombreEmpleado() << " cumple el dia " << emp2.getFecha().getDia() << " del mes " << emp2.getFecha().getMes() << "\n";
	



}

