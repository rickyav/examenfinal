#include "Fecha.h"

Fecha::Fecha()
{
	dia = 1;
	mes = 1;
}

Fecha::~Fecha()
{
}

int Fecha::getDia()
{
	return dia;
}

int Fecha::getMes()
{
	return mes;
}

void Fecha::setDia(int dia1)
{
	dia = dia1;
}

void Fecha::setMes(int mes1)
{
	mes = mes1;
}
