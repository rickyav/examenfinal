#pragma once
#include "Fecha.h"
#include <string>
using namespace std;
class Empleado
{
private:
	string nombreEmp;
	float sueldoEmp;
	int categoriaEmp;
	Fecha cumpleAniosEmp;
public:
	Empleado();
	~Empleado();
	string getNombreEmpleado();
	float getSueldoEmp();
	int getCategoriaEmp();
	Fecha getFecha();
	void setNombreEmp(string);
	void setCategoriaEmp(float);
	void setSueldo(float);
	void setFecha(Fecha);
	void promoverEmpleado(float);

	
};
