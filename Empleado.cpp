#include "Empleado.h"
#include <iostream>
using namespace std;

Empleado::Empleado()
{
	sueldoEmp = 0;
	categoriaEmp = 1;
	nombreEmp = "sin nombre";
}

Empleado::~Empleado()
{
}

string Empleado::getNombreEmpleado()
{
	return nombreEmp;
}

float Empleado::getSueldoEmp()
{
	return sueldoEmp;
}

int Empleado::getCategoriaEmp()
{
	return categoriaEmp;
}

Fecha Empleado::getFecha()
{
	return cumpleAniosEmp;
}

void Empleado::setNombreEmp(string nombre)
{
	nombreEmp = nombre;
}

void Empleado::setCategoriaEmp(float categoria)
{
	if (categoria == 4 || categoria == 3 || categoria == 2 || categoria == 1 && int(categoria) == categoria)
	{
		categoriaEmp = categoria; 
	}
	else {
		cout << "\n--> CATEGORIA NO VALIDA <--\n";
	}
}

void Empleado::setSueldo(float sueldo)
{
	sueldoEmp = sueldo;
}

void Empleado::setFecha(Fecha fecha)
{
	cumpleAniosEmp = fecha;
}

void Empleado::promoverEmpleado(float categoria)
{
	if (categoria == 4 || categoria == 3 || categoria == 2 || categoria == 1 && int(categoria) == categoria)
	{
		float diferencia = (categoria - categoriaEmp);
		if (diferencia <= 0) {
			categoriaEmp = categoria;
		}
		else if (diferencia >= 1) {
			sueldoEmp *= (1 + (diferencia * .025));
			categoriaEmp = categoria;
		}
	}
	else {
		cout << "\n--> CATEGORIA NO VALIDA <--\n";
	}
}
